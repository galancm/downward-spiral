shader_type spatial;
render_mode unshaded;

uniform vec4 color : hint_color = vec4(1.0f,1.0f,1.0f,1.0f);
uniform float fill_opacity : hint_range(0.0f,1.0f) = 0.0f;
uniform float border_width : hint_range(0.1f, 0.5f) = 0.1f;

void fragment() {
	ALBEDO = color.rgb;
	
	if (
		UV.x < 0.0f + border_width || 
		UV.x > 1.0f - border_width || 
		UV.y < 0.0f + border_width || 
		UV.y > 1.0f - border_width
	) {
		ALPHA = color.a;
	}
	else {
		ALPHA = color.a * fill_opacity;
	}
}